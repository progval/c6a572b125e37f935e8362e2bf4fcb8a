STDERR.puts "Listing backups"
Backup.find_each.map do |backup|
  print(backup.dump.url + "\n")
end

STDERR.puts "Listing imports"
Import.find_each.map do |import|
  print(import.data.url + "\n") unless import.data.url == "/data/original/missing.png"
end

STDERR.puts "Listing site uploads"
SiteUpload.find_each.map do |site_upload|
  print(site_upload.file.url + "\n")
end

STDERR.puts "Listing custom emojis"
CustomEmoji.find_each.map do |custom_emoji|
  print(custom_emoji.image.url + "\n") 
  print(custom_emoji.image.url(:static) + "\n") 
end

STDERR.puts "Listing preview cards"
PreviewCard.where.not(image_file_size: nil).find_each.map do |preview_card|
  print(preview_card.image.url + "\n") unless preview_card.image.url == "/images/original/missing.png"
end

STDERR.puts "Listing media attachments and their thumbnails"
MediaAttachment.where.not(file_file_size: nil).find_each.map do |media_attachment|
  print(media_attachment.file.url + "\n") unless media_attachment.file.url == "/files/original/missing.png"
  print(media_attachment.file.url(:small) + "\n") unless media_attachment.type == "audio" || media_attachment.file.url(:small) == "/files/original/missing.png"
  print(media_attachment.thumbnail.url + "\n") unless media_attachment.thumbnail.url == "/thumbnails/original/missing.png"
end

STDERR.puts "Listing account avatar and header"
Account.find_each.map do |account|
  print(account.avatar_original_url + "\n") unless account.avatar_original_url == "/avatars/original/missing.png"
  print(account.avatar_static_url + "\n") unless account.avatar_static_url == "/avatars/original/missing.png"
  print(account.header_original_url + "\n") unless account.header_original_url == "/headers/original/missing.png"
  print(account.header_static_url + "\n") unless account.header_static_url == "/headers/original/missing.png"
end